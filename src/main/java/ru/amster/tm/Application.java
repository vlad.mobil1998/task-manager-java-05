package ru.amster.tm;

import ru.amster.tm.constant.TerminalConst;

public class Application {

    public static void main(final String[] args) {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        parseArgs(args);
    }

    public static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        if (TerminalConst.HELP.equals(arg)) {
            showHelpMsg(arg);
        }
        if (TerminalConst.ABOUT.equals(arg)) {
            showAboutMsg(arg);
        }
        if (TerminalConst.VERSION.equals(arg)) {
            showVersionMsg(arg);
        }
    }

    public static void showHelpMsg(String arg) {
        System.out.println(TerminalConst.ABOUT + " - Show developer info.");
        System.out.println(TerminalConst.VERSION + " - Show version info.");
        System.out.println(TerminalConst.HELP + " - Display terminal commands.");
    }

    public static void showAboutMsg(String arg) {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Amster Vladislav");
        System.out.println("E-MAIL: vlad@amster.ru");
    }

    public static void showVersionMsg(String arg) {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
